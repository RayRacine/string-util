;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; String Util Library
;; Copyright (C) 2007-2013  Raymond Paul Racine
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#lang typed/racket/base

(provide:
 [null-string? (String -> Boolean)]
 [default-string (String String -> String)]
 [starts-with-char? (String Char -> Boolean)]
 [ends-with-char? (String Char -> Boolean)]
 [starts-with? (String String -> Boolean)]
 [string-first-char-occurrence (String Char -> (Option Index))]
 [string-common-prefix-length (String String -> Index)]
 [string-common-prefix  (String String -> String)]
 [string-tokenize       (String Char-Set -> (Listof String))]
 [weave-string-separator (String (Listof String) -> String)]
 [substring-trim-spaces (String Integer Integer -> String)])

(require
 (only-in list-util
          weave)
 (only-in opt/option
          opt-apply-orelse)
 (only-in typed/srfi/14
	  Char-Set))

(require/typed srfi/13
  (string-tokenize (String Char-Set -> (Listof String))))

(: null-string? (String -> Boolean))
(define (null-string? s)
  (if (string? s)
      (zero? (string-length s))
      #f))

(: default-string (String String -> String))
(define (default-string str default)
  (if (null-string? str)
      default
      str))

(: starts-with-char? (String Char -> Boolean))
(define (starts-with-char? s ch)
  (if (zero? (string-length s))
      #f
      (char=? (string-ref s 0) ch)))

(: ends-with-char? (String Char -> Boolean))
(define (ends-with-char? s ch)
  (let ((len (string-length s)))
    (if (zero? len)
        #f
        (char=? (string-ref s (sub1 len)) ch))))

(: starts-with? (String String -> Boolean))
(define (starts-with? s prefix)
  (let ((plen (string-length prefix)))
    (if (>= (string-length s)
            plen)
        (string=? (substring s 0 plen) prefix)
        #f)))

(: string-first-char-occurrence (String Char -> (Option Index)))
(define (string-first-char-occurrence s ch)
  (let ((len (string-length s)))
    (let loop ((i 0))
      (if (eq? i len)
          #f
          (if (char=? (string-ref s i) ch)
              (assert i index?)
              (loop (add1 i)))))))

(: string-common-prefix-length (String String -> Index))
(define (string-common-prefix-length prefix s)
  (let: ((len (min (string-length s)
                   (string-length prefix))))
    (let loop ((i 0))
      (if (and (< i len)
               (char=? (string-ref prefix i)
                       (string-ref s      i)))
          (loop (add1 i))
          (assert i index?)))))

(: string-common-prefix (String String -> String))
(define (string-common-prefix prefix s)
  (substring s (string-common-prefix-length prefix s)))

;  (opt-apply-orelse (string-common-prefix-index prefix s) (λ: ((i : Index)) (substring s i)) s))

;; weave a separator string strictly between every pair of strings in a list
;; (weave "," '("1" "2" "3")) -> "1,2,3"
(: weave-string-separator (String (Listof String) -> String))
(define weave-string-separator
  (lambda (sep lst)
    (apply string-append (weave sep lst))))

;; Extract a substring, trimming spaces
(: substring-trim-spaces (String Integer Integer -> String))
(define (substring-trim-spaces src-str start end)
  (let ((s-pos (do: : Integer ((s-pos start (add1 s-pos)))
		    ((or (eqv? s-pos end)
			 (not (eqv? (string-ref src-str s-pos) #\space)))
		     s-pos)))
	(e-pos (do: : Integer  ((e-pos end (sub1 e-pos)))
		    ((or (eqv? e-pos start)
			 (not (eqv? (string-ref src-str e-pos) #\space)))
		     (if (< e-pos end)
			 (add1 e-pos)
			 e-pos)))))
    (substring src-str s-pos e-pos)))
