#lang scribble/manual
@require[@for-label[string-util
                    racket/base]]

@title{string-util}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@section{String Utilities}
@defmodule[string-util]

@defproc[(null-string? [s String]) Boolean]{
@racket[(String -> Boolean)]

Is the string the empty string?
}

@defproc[(default-string [s String] [default String]) String]{
@racket[(-> String String String)]

If given string @racket[s] is the @racket[null-string?] return the default @racket[String],
otherwise the @racket[String] @racket[s].
}

@defproc[(starts-with-char? [s String] [ch Char]) Boolean]{
@racket[(String Char -> Boolean)]

Does the string start with the char.
}

@defproc[(ends-with-char? [s String] [ch Char]) Boolean]{
@racket[(String Char -> Boolean)]

Does the string end with the char.
}

@defproc[(starts-with? [s String] [prefix String]) Boolean]{
@racket[(String String -> Boolean)]

Does the string start with the given prefix.
}

@defproc[(string-first-char-occurrence [s String] [ch Char]) (Option Index)]{
@racket[(String Char -> (Option Index))]
 
Returns the index of the first occurrence of the character in the string scanning from left to right.
Returns @racket[#f] if the character is not found in the string.
}

@defproc[(string-common-prefix-length [s1 String] [s2 String]) Index]{
@racket[(String String -> Index)]

Given two strings returns the index of their longest common prefix from left to right.
In other words, the first index position before the two strings start to diverge.
}

@defproc[(substring-common-prefix [s1 String] [s2 String]) String]{
@racket[(String String -> String)]

Returns the common prefix string from left to right between two strings.
}

@defproc[(string-tokenize [s String] [delims Char-Set]) (Listof String)]{
@racket[(String Char-Set -> (Listof String))]

Split the string into tokens using the set of char delimiters.
}

@defproc[(weave-string-separator [sep String] [lst (Listof String)]) String]{
@racket[(String (Listof String) -> String)]

Weave a the separator string between each string in the list and then reduce to a single string value.
}

For example:

@racketblock[
(weave-string-separator "," '("1" "2" "3"))
"1,2,3"

(weave-string-separator "," '("1"))
"1"
]

@defproc[(substring-trim-spaces [s String] [start-pos Integer] [end-pos Integer]) String]{
@racket[(: substring-trim-spaces (String Integer Integer -> String))]

Extracts a substring from the start index inclusive to the end index exclusive.  Spaces are trimmed from both ends.
}
